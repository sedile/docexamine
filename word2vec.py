import gensim
from gensim.models import Word2Vec
import multiprocessing
import re
import sys

class Word2Vec:

    def __init__(self):
        ''' wordlist from java'''
        self.__wordlist = []
        ''' keywords from each document '''
        self.__keywordmap = {}
        ''' thr result from word2vec for each keyword '''
        self.__relatedlist = []
        ''' files from and to java '''
        self.__dirs = []
        self.__dirs.append(str(sys.argv[1]))
        self.__dirs.append(str(sys.argv[2]))
        self.__dirs.append(str(sys.argv[3]))

    def readWordlist(self):
        wordfile = open(self.__dirs[0],'r')
        for word in wordfile:
            word = re.sub('\\n','',word)
            self.__wordlist.append(word)

    def readKeywords(self):
        keys = open(self.__dirs[1],'r')
        i = 0
        for term in keys:
            klist = term.rstrip().split(' ')
            self.__keywordmap.update({i:klist})
            i = i + 1

    def saveResult(self):
        save = open(self.__dirs[2],'w')
        for keylist in self.__relatedlist:
            for relword in keylist:
                save.write(relword + ' ')
            save.write('\n')

    def collectRelatedWords(self):
        for value in self.__keywordmap.values():
            resultlist = []
            for word in value:
                relword = self.__getMostNSimilar(word,1)
                resultlist.append(relword[0][0])
            self.__relatedlist.append(resultlist)

    def initModel(self):
        self.__model = gensim.models.Word2Vec([self.__wordlist], size=128, window=10, min_count=1, sample=1e-3, workers=multiprocessing.cpu_count())      

    def trainModel(self):
        self.__model.train(self.__wordlist, total_examples=len(self.__wordlist), epochs=128)

    def __getMostNSimilar(self,word,n):
        return self.__model.most_similar(positive=word, topn=n)

if __name__ == "__main__":
    w2v = Word2Vec()
    w2v.readWordlist()
    w2v.readKeywords()
    w2v.initModel()
    w2v.trainModel()
    w2v.collectRelatedWords()
    w2v.saveResult()
