package unittest;

import org.junit.Assert;
import org.junit.Test;

import util.WordPair;

public class WordPairTest {

	@Test
	public void empty_wordpair(){
		WordPair pair = new WordPair("","");
		Assert.assertEquals("", pair.getLeftElement());
		Assert.assertEquals("", pair.getRightElement());
	}
	
	@Test
	public void rightwrong_wordpair(){
		WordPair pair = new WordPair("haus","baum");
		Assert.assertEquals("haus", pair.getLeftElement());
		Assert.assertNotEquals("right is false", "Baum", pair.getRightElement());
	}
	
	@Test
	public void leftwrong_wordpair(){
		WordPair pair = new WordPair("haus","baum");
		Assert.assertNotEquals("left is false", "Haus", pair.getRightElement());
		Assert.assertEquals("baum", pair.getRightElement());
	}
	
	@Test
	public void equal_wordpair(){
		WordPair pair = new WordPair("haus","haus");
		Assert.assertEquals(pair.getLeftElement(), pair.getRightElement());
	}
	
	@Test
	public void notEqual_wordpair(){
		WordPair pair = new WordPair("Haus","haus");
		Assert.assertNotEquals(pair.getLeftElement(), pair.getRightElement());
	}

}
