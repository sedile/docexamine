package unittest;

import org.junit.Assert;
import org.junit.Test;

import algorithm.Pagerank;

import util.Matrix;

public class PageRankTest {
	
	private Matrix matrix;
	private final double DAMPINGFACTOR = 0.85;
	final float[][] init = {{0,0.5f,0.5f},{0,0,1},{1,0,0}}; // test matrix
	private double[] pagerank = {DAMPINGFACTOR,DAMPINGFACTOR,DAMPINGFACTOR};

	@Test
	public void zeroIterationTest(){
		matrix = new Matrix(init,3);
		
		// pagerank with zero iteration //
		Pagerank pr = new Pagerank(matrix, DAMPINGFACTOR, 0);
		pr.calculatePagerank();
		double[] expected = {DAMPINGFACTOR,DAMPINGFACTOR,DAMPINGFACTOR};
		pagerank = pr.getPagerankVector();
		Assert.assertArrayEquals(expected, pagerank, 0);
	}
	
	@Test
	public void oneIterationTest() {
		matrix = new Matrix(init,3);
		
		// pagerank with one iteration //
		Pagerank pr = new Pagerank(matrix, DAMPINGFACTOR, 1);
		pr.calculatePagerank();
		double[] expected = {0.85,0.425,1.275};
		pagerank = pr.getPagerankVector();
		Assert.assertArrayEquals(expected, pagerank, 0.01);
	}

	@Test
	public void twoIterationTest() {
		matrix = new Matrix(init,3);
		
		// pagerank with two iteration //
		Pagerank pr = new Pagerank(matrix, DAMPINGFACTOR, 2);
		pr.calculatePagerank();
		double[] expected = {1.275,0.425,0.85};
		pagerank = pr.getPagerankVector();
		Assert.assertArrayEquals(expected, pagerank, 0.01);
	}
	
	@Test
	public void fiveIterationTest() {
		matrix = new Matrix(init,3);
		
		// pagerank with five iteration //
		Pagerank pr = new Pagerank(matrix, DAMPINGFACTOR, 5);
		pr.calculatePagerank();
		double[] expected = {1.0625,0.53125,0.95625};
		pagerank = pr.getPagerankVector();
		Assert.assertArrayEquals(expected, pagerank, 0.01);
	}
}
