package unittest;

import org.junit.Assert;
import org.junit.Test;

import algorithm.Jaro;

public class JaroTest {

	private Jaro jaro = new Jaro();

	@Test
	public void null_test() {
		double similiarity = jaro.similarity(null, null);
		Assert.assertEquals(0.0, similiarity, 0);
	}
	
	@Test
	public void empty_test() {
		final String x = new String();
		final String y = new String();
		double similiarity = jaro.similarity(x,y);
		Assert.assertEquals(0.0, similiarity, 0);
	}
	
	@Test
	public void equals_test() {
		final String x = new String("haus");
		final String y = new String("haus");
		double similiarity = jaro.similarity(x,y);
		Assert.assertEquals(1.0, similiarity, 0);
	}
	
	@Test
	public void similiarity_test(){
		final String x = new String("bahn");
		final String y = new String("auto");
		double similiarity = jaro.similarity(x,y);
		System.out.println(similiarity);
		Assert.assertEquals(0.5, similiarity, 0.001);
	}

}

