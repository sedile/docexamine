package unittest;

import org.junit.Assert;
import org.junit.Test;

import util.Graph;

public class GraphTest {

	@Test
	public void insertNodes(){
		String a = new String("auto");
		String b = new String("haus");
		String c = new String("salz");
		
		Graph g = new Graph(0);
		
		g.insertNode(a); // ID = 0
		g.insertNode(b);
		g.insertNode(c); // ID = 2
		
		Assert.assertEquals("auto", g.getNodeWordByID(0));
		Assert.assertNotEquals("salz", g.getNodeWordByID(1));
		Assert.assertEquals("salz", g.getNodeWordByID(2));
	}
	
	@Test
	public void insertEdges(){
		String a = new String("auto");
		String b = new String("haus");
		String c = new String("salz");
		String d = new String("kiwi");
		String e = new String("obst");
		String f = new String("garten");
		String h = new String("isoliert");
		
		Graph g = new Graph(0);
		
		g.insertNode(a);
		g.insertNode(b);
		g.insertNode(c);
		g.insertNode(d);
		g.insertNode(e);
		g.insertNode(f);
		g.insertNode(h);

		g.insertEdge(a,b);
		g.insertEdge(a,d);
		g.insertEdge(b,e);
		g.insertEdge(e,c);
		g.insertEdge(e,d);
		g.insertEdge(e,h);
		g.insertEdge(d,b);
		
		System.out.println(g.getGraphAsString());
		
		// get the adjacency matrix to count the number of outgoing edges
		float[][] adj = g.transformIntoAdjacencMatrix();
		
		int out = 0;
		for(int i = 0; i < adj.length; i++){
			if ( adj[0][i] == 1){
				out++;
			}
		}
		
		Assert.assertEquals(2, out); // auto -> haus , kiwi
		
		out = 0;
		for(int i = 0; i < adj.length; i++){
			if ( adj[1][i] == 1){
				out++;
			}
		}
		
		Assert.assertEquals(1, out); // haus -> obst
		
		out = 0;
		for(int i = 0; i < adj.length; i++){
			if ( adj[2][i] == 1){
				out++;
			}
		}
		
		Assert.assertEquals(0, out); // salz -> NIL
		
		out = 0;
		for(int i = 0; i < adj.length; i++){
			if ( adj[3][i] == 1){
				out++;
			}
		}
		
		Assert.assertEquals(1, out); // kiwi -> haus
		
		out = 0;
		for(int i = 0; i < adj.length; i++){
			if ( adj[4][i] == 1){
				out++;
			}
		}
		
		Assert.assertEquals(3, out); // obst -> salz , kiwi , isoliert
		
		out = 0;
		for(int i = 0; i < adj.length; i++){
			if ( adj[5][i] == 1){
				out++;
			}
		}
		
		Assert.assertEquals(0, out); // garten -> NIL
		
		out = 0;
		for(int i = 0; i < adj.length; i++){
			if ( adj[6][i] == 1){
				out++;
			}
		}
		
		Assert.assertEquals(0, out); // isoliert -> NIL
	}

}
