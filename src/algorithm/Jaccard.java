package algorithm;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * This algorithm is similar to the dice coefficient. It compares two
 * Sets and returns the similarity of the two sets as a double value
 * @author sebastian
 */
public class Jaccard implements ContentComparer<Map<String, Double>>, Callable<Double[]>{
	
	private Map<String, Double> _x;
	private Map<String, Double> _y;
	private int _docI;
	private int _docJ;
	
	/**
	 * the deafult constructor
	 */
	public Jaccard() {}
	
	/**
	 * Default Constructor for ExecutorService
	 * @param x set one
	 * @param y set two
	 * @param i document with index i
	 * @param j document with index j
	 */
	public Jaccard(Map<String,Double> x, Map<String,Double> y, int i ,int j) {
		_x = x;
		_y = y;
		_docI = i;
		_docJ = j;
	}
	
	@Override
	public double similarity(final Map<String, Double> x, final Map<String, Double> y){
		if ( x == null || y == null ){
			System.err.println("one or both vectors are null");
			return 0.0;
		}
		
		/* create the union of the two keysets from the vectors */
		Set<String> union = new HashSet<String>(x.keySet());
		union.addAll(y.keySet());
		
		if ( union.isEmpty() ){
			return 0.0;
		}
		
		int inter = x.keySet().size() + y.keySet().size() - union.size();
		return (double)(inter)/(union.size());
	}

	@Override
	public Double[] call() throws Exception {
		Double[] values = new Double[3];
		values[0] = (double) _docI;
		values[1] = (double) _docJ;
		values[2] = similarity(_x, _y);
		return values;
	}

}
