package algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import util.Graph;
import util.Matrix;
import util.TempData;
import util.WordPair;

/**
 * This class calculates the pagerank of a given matrix
 * @author sebastian
 */
public class Pagerank implements Callable<TempData> {
	
	private List<String> _data;
	private Matrix _matrix;
	private double[] _factorvector;
	private final double DAMPING;
	private final int ITERATIONS;
	private final int IDX;
	private final int NUMBER_OF_KEYWORDS;
	
	/**
	 * Constructor to initilize the Pagerank algorithm
	 * @param data words from a document
	 * @param idx document id
	 * @param numOfKeywords number of keywords
	 * @param damping damping factor
	 * @param iterations number of iterations
	 */
	public Pagerank(List<String> data, int idx, int numOfKeywords, double damping, int iterations){
		_data = data;
		IDX = idx;
		DAMPING = damping;
		NUMBER_OF_KEYWORDS = numOfKeywords;
		ITERATIONS = iterations;
	}
	
	/**
	 * Constructor to initilize the Pagerank algorithm
	 * @param adjMatrix the initial matrix
	 * @param damping dampingfactor between [0;1] (0.85 recommended)
	 * @param iterations amount of iterations to calculate the PR
	 */
	public Pagerank(final Matrix adjMatrix, final double damping, final int iterations){
		IDX = -1;
		NUMBER_OF_KEYWORDS = 0;
		DAMPING = damping;
		_matrix = adjMatrix;
		_factorvector = new double[_matrix.getSize()];
		Arrays.fill(_factorvector, DAMPING);
		ITERATIONS = iterations;
	}
	
	/**
	 * this function calculates the pagerank
	 */
	public void calculatePagerank(){
		_matrix.transpose();
		for(byte i = 0; i < ITERATIONS; i++){
			_factorvector = _matrix.multiply(_factorvector);
		}
	}
	
	/**
	 * return the pagerankvector
	 * @return pagerankvector
	 */
	public double[] getPagerankVector(){
		return _factorvector;
	}

	@Override
	public TempData call() throws Exception {
		
		/* 1. Built Co occurrences of words */
		Cooccurrence cooc = new Cooccurrence(_data);
		cooc.builtCooccurrencesWindow();
		Map<WordPair, Integer> wordpair = cooc.getWordPairs();
		
		Set<String> no_dupl = new HashSet<String>();
		for(Map.Entry<WordPair, Integer> e : wordpair.entrySet()){
			no_dupl.add(e.getKey().getLeftElement());
			no_dupl.add(e.getKey().getRightElement());
		}
		
		/* 2. Insert all words/nodes into the graph */
		Graph g = new Graph(NUMBER_OF_KEYWORDS);
		for(String node : no_dupl){
			g.insertNode(node);
		}
		
		/* 3. Connect relevant nodes with edges */
		for(Map.Entry<WordPair, Integer> nodes : wordpair.entrySet()){
			g.insertEdge(nodes.getKey().getLeftElement(), nodes.getKey().getRightElement());
		}
	
		//System.out.println(g.getGraphAsString());
		
		/* 4. Create a weight matrix */
		float[][] m = g.transformIntoWeightMatrix();
		_matrix = new Matrix(m, m.length);
		_factorvector = new double[_matrix.getSize()];
		Arrays.fill(_factorvector, DAMPING);
		//matrix.printMatrix();
		
		/* 5. Initialize PAGERANK */
		calculatePagerank();
		double[] weights = getPagerankVector();
		
		/* 6. Search the n most weighted words */
		List<String> keywordlist = new ArrayList<String>();
		List<Double> keyweights = new ArrayList<Double>();
		
		int runs = 0;
		if ( weights.length < NUMBER_OF_KEYWORDS ){
			runs = weights.length;
		} else {
			runs = NUMBER_OF_KEYWORDS;
		}
		
		for(int i = 0; i < runs; i++){
			double max = 0.0;
			int maxIdx = 0;
			for(int j = 0; j < weights.length; j++){
				if ( weights[j] > max){
					max = weights[j];
					maxIdx = j;
				}
			}
			keywordlist.add(g.getNodeWordByID(maxIdx));
			keyweights.add(max);
			weights[maxIdx] = 0.0;
		}
		
		TempData temp = new TempData(IDX);
		
		for(int i = 0; i < keywordlist.size(); i++){
			temp.getTermVector().addTerm(keywordlist.get(i), keyweights.get(i));
		}
		
		temp.getKeywordSet().addAll(keywordlist);
		return temp;
	}

}
