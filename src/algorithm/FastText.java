package algorithm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implements the fasttext algorithm. Requires the 'fasttext.py'
 * Pythonscript to work
 * @author sebastian
 */
public class FastText {
	
	private Set<String> _wordlist;
	private List<Set<String>> _keywords;
	private List<Set<String>> _relatedkeys;
	
	private final String WORDLISTFILE;
	private final String KEYWORDFILE;
	private final String RELATEDFILE;
	
	/**
	 * Constructor
	 * @param wordlist wordlist contains all words of all documents
	 * @param keywords contains all keywords from each document
	 */
	public FastText(final Set<String> wordlist, final List<Set<String>> keywords, final String wordlistfile, final String keywordfile, final String relatedfile){
		WORDLISTFILE = wordlistfile;
		KEYWORDFILE = keywordfile;
		RELATEDFILE = relatedfile;
		_wordlist = wordlist;
		_keywords = keywords;
		_relatedkeys = new ArrayList<Set<String>>();
	}
	
	/**
	 * saves the keywords temporarily for the pythonscript
	 */
	public void saveWordlist(){
		try {
			/* TMP_WORDLIST_PATH */
			FileWriter fw = new FileWriter(WORDLISTFILE);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(String kw : _wordlist ){
				bw.write(kw+"\n");
			}
			bw.flush();
			bw.close();
			fw.close();
		} catch ( IOException ioe ){
			ioe.printStackTrace();
		}
	}
	
	/**
	 * saves the keywords temporarily for the pythonscript
	 */
	public void saveKeywords(){
		try {
			/* TMP_KEYWORD_PATH */
			FileWriter fw = new FileWriter(KEYWORDFILE);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(Set<String> kw : _keywords ){
				for(String keyword : kw ){
					bw.write(keyword+" ");
				}
				bw.write("\n");
			}
			bw.flush();
			bw.close();
			fw.close();
		} catch ( IOException ioe ){
			ioe.printStackTrace();
		}
	}
	
	/**
	 * collects all related words from the word2vec algorithm
	 * @return list contains all related words for each document
	 */
	private List<Set<String>> collectRelatedKeywords() throws IOException {
		List<Set<String>> relatedkeys = new ArrayList<Set<String>>();
		int counter = 60;
		while(counter > 0){
			try {
				File file = new File(RELATEDFILE);
				if ( !file.exists() ){
					counter--;
					
					if ( counter == 0){
						System.out.println("find related keywords ... failed");
						System.out.println("[ERROR] out of time");
						return relatedkeys;
					}
				} else {
					break;
				}
				
				/* wait max 5 minutes : 5 sec * 60 tries */
				Thread.sleep(5000);
			} catch ( InterruptedException ie){
				ie.printStackTrace();
				return relatedkeys;
			}
		}

		/* TMP_RELWORD_PATH */
		FileReader fr = new FileReader(RELATEDFILE);
		BufferedReader br = new BufferedReader(fr);
			
		boolean read = true;
		while( read ){
			String line = br.readLine();
				
			if ( line == null ){
				read = false;
				break;
			}
				
			relatedkeys.add(new HashSet<String>());
			String[] relKeys = line.split(" ");				
			for(String w : relKeys ){
				relatedkeys.get(relatedkeys.size() - 1).add(w);
			}
		}
			
		br.close();
		fr.close();
		return relatedkeys;
	}
	
	/**
	 * Builts a processbuilder to start the word2vec from python
	 */
	public void start(){
		// In einem Thread, da sonst die rel-datei fehlt (wie lange warten?)

		ProcessBuilder pb = new ProcessBuilder("python","fasttext.py",WORDLISTFILE,KEYWORDFILE,RELATEDFILE);
		try {
			pb.start();
			System.out.println("PYTHON gestartet ...");		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * transforms the result from python
	 * @return related words
	 */
	public List<Set<String>> relwordsToDocument(){
		try {
			List<Set<String>> result = collectRelatedKeywords();
			return result;
		} catch ( IOException ioe ){
			System.err.println("Could not collect related keywords from python (timeout)");
			ioe.printStackTrace();
			return new ArrayList<Set<String>>();
		} finally {
			File wordlist = new File(WORDLISTFILE);
			File keywordfile = new File(KEYWORDFILE);
			File relatedfile = new File(RELATEDFILE);
			wordlist.delete();
			keywordfile.delete();
			relatedfile.delete();
		}
	}
	
	/**
	 * returns the related words for each document if available
	 * @return related words
	 */
	public List<Set<String>> getRelatedKeys(){
		return _relatedkeys;
	}

}
