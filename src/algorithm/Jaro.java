package algorithm;

import java.util.concurrent.Callable;

/**
 * A implementation of the jaro-weight to calculate the similarity
 * between two strings
 * @author sebastian
 */
public class Jaro implements ContentComparer<String>, Callable<String[]> {
	
	private String _x;
	private String _y;
	private int _docID;
	
	/**
	 * Default Constructor for ExecutorService
	 * @param x word one
	 * @param y word two
	 * @param idx document id
	 */
	public Jaro(String x, String y, int idx) {
		_x = x;
		_y = y;
		_docID = idx;
	}
	
	/**
	 * the default constructor
	 */
	public Jaro() {}
	
	@Override
	public double similarity(final String x, final String y){
		
		if ( x == null || y == null ){
			return 0.0;
		}
		
		/* both words are 'empty' */
		if ( x.length() == 0 && y.length() == 0){
			return 0.0;
		}
		
		/* both words are the same */
		if ( x.equals(y) ){
			return 1.0;
		}
		
		int distance = Integer.max(x.length(), y.length()) / 2 - 1;
		
		/* to compare each character of the word */
		boolean[] x_matches = new boolean[x.length()];
		boolean[] y_matches = new boolean[y.length()];
		int match = 0;
		int disposition = 0;
		
		for(int i = 0; i < x.length(); i++){
			int begin = Integer.max(0, (i - distance));
			int end = Integer.min((i + distance + 1), y.length());
			
			for(int j = begin; j < end; j++){
				if ( y_matches[j]){
					continue;
				}
				
				if ( x.charAt(i) != y.charAt(j)){
					continue;
				}
				
				x_matches[i] = true;
				y_matches[j] = true;
				match++;
				break;
			}
		}
		
		/* no character matching*/
		if( match == 0){
			return 0.0;
		}
		
		int k = 0;
		for(int i = 0; i < x.length(); i++){
			if ( !x_matches[i] ){
				continue;
			}
			
			while( !y_matches[k]){
				k++;
			}
			
			if ( x.charAt(i) != y.charAt(k) ){
				disposition++;
			}
			k++;
		}
		
		double score = (((double) match / x.length()) + 
					((double) match / y.length()) +
					(((double) match - disposition/2.0) / match)) / 3.0;
		return score;
	}

	@Override
	public String[] call() throws Exception {
		String[] values = new String[4];
		values[0] = new String(""+_docID);
		values[1] = _x;
		values[2] = _y;
		values[3] = Double.toString(similarity(_x, _y));
		return values;
	}
}
