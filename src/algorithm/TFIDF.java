package algorithm;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import util.TermVector;
import util.ThreadHelper;

/**
 * A implementation of the term-frequency-inverted-document-frequency (TF-IDF)
 * algorithm
 * @author sebastian
 *
 */
public class TFIDF implements Callable<ThreadHelper<TermVector>> {
	
	private List<List<String>> _documents;
	private List<String> _document;
	private final int IDX;
	
	/**
	 * Default Constructor for ExecutorService
	 * @param docs all documents
	 * @param doc one document from the documentpool
	 * @param idx document id
	 */
	public TFIDF(List<List<String>> docs, List<String> doc, int idx) {
		_documents = docs;
		_document = doc;
		IDX = idx;
	}
	
	/**
	 * Default Constructor
	 */
	public TFIDF() {
		IDX = 0;
	}
	
    /**
     * computes the term-frequency of a word in the document
     * @param document the textdocument
     * @param term term to be examined
     * @return weightvalue of the term
     */
    public double computeTF(final List<String> document, final String term){
        int hit = 0;
        for(String word : document){
            if(word.equalsIgnoreCase(term)){
                hit++;
            }
        }
        
        return (hit/(double) document.size()); 
    }

    /**
     * computes if the term is contained in other documents
     * @param documents pool of textdocuments
     * @param term term to be examined
     * @return the inverse document frequency value
     */
    public double computeIDF(final List<List<String>> documents, final String term){
        int hit = 0;
        for(List<String> document : documents){
            for(String word : document){
                if(word.equalsIgnoreCase(term)){
                    hit++;
                    break;
                }
            }
        }
        
        if ( hit == 0 ){
        	return 0.0;
        }
        
        return (Math.log10(documents.size()/hit));
    }

    /**
     * computes the TF*IDF-Value of a term
     * @param document one specified document
     * @param documents the documentpool
     * @param term term to be examined
     * @return weight of the term
     */
    public double computeTFIDF(final List<String> document, final List<List<String>> documents, final String term){
    	return computeTF(document,term) * computeIDF(documents,term);
    }

	@Override
	public ThreadHelper<TermVector> call() throws Exception {
		ThreadHelper<TermVector> helper = new ThreadHelper<TermVector>(new TermVector(),IDX);
		
		/* computes the term-frequency of a word in a document */
		Map<String, Double> termfrequency = new HashMap<String, Double>();
		for(String word : _document){
			termfrequency.put(word, new Double(Collections.frequency(_document, word)/(double) _document.size()));
		}
		
		for(Map.Entry<String, Double> elem : termfrequency.entrySet()){
			double weight = elem.getValue() * computeIDF(_documents, elem.getKey());
			if ( weight > 0.0 ){
				helper.getData().addTerm(elem.getKey(), weight);
			}
		}
		
		return helper;
	}

}
