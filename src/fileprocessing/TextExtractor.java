package fileprocessing;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * A interface for the textdata extraction classes
 * @author sebastian
 *
 */
public interface TextExtractor extends Callable<TextExtractor> {

	/**
	 * extracts the content of a file
	 * @return true, if there is content
	 */
	public boolean extractText();
	
	/**
	 * sets optional a file which contains stopwords
	 * @param stopwordpath the filepath to the stopwords
	 */
	public void setStopwordFilepath(final String stopwordpath);
	
	/**
	 * removes stopwords from the wordlist
	 * @throws file not found, no stopwords will be removed
	 */
	public void removeStopwords() throws FileNotFoundException;

	/**
	 * calculates a hashvalue from the textcontent
	 * @param text text of the document
	 * @return hash as string
	 */
	public String hash(final String text);
	
	/**
	 * returns the inital content for the json file
	 * @return jsoncontent
	 */
	public Map<String, String> getJsonContent();
	
	/**
	 * returns all words from one document
	 * @return words from a document
	 */
	public List<String> getDocumentContent();
}
