package fileprocessing;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * This class is responsible to collect all textdatafiles from a directory
 * @author sebastian
 */
public class FileCollector {
	
	private final int NUMBER_OF_THREADS;
	private String _stopwordpath;
	private Map<File, String> _filemap;
	private List<List<String>> _data;
	private List<Map<String, String>> _jsoncollector;
	
	/**
	 * The constructor gets the filepath where the textfiles lies 
	 * @param dir path to the documents
	 * @param stopwordfile path to the stopwords
	 * @param workers number of threads
	 */
	public FileCollector(final String dir, final String stopwordfile, int workers){
		if ( workers <= 0){
			NUMBER_OF_THREADS = 2;
		} else {
			NUMBER_OF_THREADS = workers;
		}
		_data = new ArrayList<List<String>>();
		_stopwordpath = stopwordfile;
		if ( dir.isEmpty() ){
			System.err.println("[ERROR] path is empty");
			return;
		}
		collectFiles(dir);
	}
	
	/**
	 * collect all files in a array
	 * @param dir directory to the documents
	 */
	private void collectFiles(final String dir){
		File directory = new File(dir);
		if ( !directory.isDirectory() ){
			System.err.println("[ERROR] path could not find or is not a directory");
			return;
		}
			
		File[] filelist = directory.listFiles();	
		_filemap = new HashMap<File, String>();
		_jsoncollector = new ArrayList<Map<String, String>>();
		for(int i = 0; i < filelist.length; i++){
			String[] extension = filelist[i].getName().split("\\.");
			System.out.println("Reading document '" + filelist[i].getName() + "' ...");
			_filemap.put(filelist[i], extension[extension.length - 1].toLowerCase());
		}
	}
	
	/**
	 * extract all data from the documents
	 */
	public void collectDocumentContent(){
		
		if ( _filemap == null || _filemap.isEmpty()){
			System.err.println("no file(s) available");
			return;
		}

		List<Future<TextExtractor>> tasks = new ArrayList<Future<TextExtractor>>();
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		for(Map.Entry<File, String> elem : _filemap.entrySet() ){
			String ext = elem.getValue();
			if( ext.equals("txt") ){
				TextExtractor plain = new PlainExtractor(elem.getKey());
				plain.setStopwordFilepath(_stopwordpath);
				final Future<TextExtractor> future = exec.submit(plain);
				tasks.add(future);
			} else if ( ext.equals("html") ){
				TextExtractor html = new HTMLExtractor(elem.getKey());
				html.setStopwordFilepath(_stopwordpath);
				final Future<TextExtractor> future = exec.submit(html);
				tasks.add(future);
			} else if ( ext.equals("pdf") ){
				TextExtractor pdf = new PDFExtractor(elem.getKey());
				pdf.setStopwordFilepath(_stopwordpath);
				final Future<TextExtractor> future = exec.submit(pdf);
				tasks.add(future);
			}
		}
		
		List<TextExtractor> result = new ArrayList<TextExtractor>();
		for(Future<TextExtractor> f : tasks){
			try {
				TextExtractor te = f.get(120, TimeUnit.SECONDS);
				if ( !te.getDocumentContent().isEmpty() ){
					result.add(te);
				}
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				e.printStackTrace();
				System.err.println("error by collecting data from document");
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}

		for(TextExtractor t : result){
			_data.add(t.getDocumentContent());
			_jsoncollector.add(t.getJsonContent());
		}
	}
	
	/**
	 * returns the inital content for the jsonfile
	 * @return a list with a map. the key of this map represents the keyvalue
	 * from the jsonfile and the value the values of the jsonfile
	 */
	public List<Map<String, String>> getJsonContent(){
		return _jsoncollector;
	}
	
	public List<String> getFileNames(){
		List<String> filenames = new ArrayList<String>();
		for(Map.Entry<File, String> elem : _filemap.entrySet()){
			System.out.println(elem.getKey().getAbsolutePath());
			filenames.add(elem.getKey().getAbsolutePath());
		}
		return filenames;
	}
	
	/**
	 * returns all words of all documents
	 * @return list containing all words of all documents
	 */
	public List<List<String>> getDocumentContent(){
		return _data;
	}
}
