package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class provides functions to built a JSON-File
 * @author sebastian
 *
 */
public class JSONBuilder {

	private String _jsonpath, _filename;
	private List<JSONObject> _json;
	private JSONArray _result;
	
	/**
	 * Constructor
	 * @param jsonpath path where the jsonfile will be saved when no connection
	 * the the Database is possible
	 * @param jsonfilename name for the jsonfile
	 */
	public JSONBuilder(final String jsonpath, final String jsonfilename){
		_jsonpath = jsonpath;
    	_filename = jsonfilename;
    	_json = new ArrayList<JSONObject>();
	}
	
	/**
	 * creates a new JSON Object
	 */
	public void newDocument(){
		_json.add(new JSONObject());
	}
	
	/**
	 * inserts one attribute into the jsonfile
	 * @param key key
	 * @param value value or values
	 * @param idx document number where to place the attribute
	 */
	public void insertAttribute(final String key, final Object value, int idx){
		_json.get(idx).put(key, value);
	}
	
	/**
	 * megres all JSON Object into one JSON Array
	 */
	public void wrapDocuments(){
		_result = new JSONArray();
		
		for(JSONObject doc : _json){
			_result.put(doc);
		}
	}
	
	/**
	 * returns the JSON file as a String
	 * @return jsonstring
	 */
	public String getJSONString(){
		return new String(_result.toString());
	}
	
	/**
	 * for Debugging : prints the jsoncontent
	 */
	public void print(){
		System.out.println(_result);
	}

	/**
	 * saves the jsonfile
	 */
    public void saveJson(){
    	File json = new File(_jsonpath + _filename);
    	
    	try {
    		FileWriter fw = new FileWriter(json);
    		BufferedWriter bw = new BufferedWriter(fw);
    		bw.write(_result.toString());
    		bw.flush();
    		bw.close();   		
    	} catch ( IOException ioe ){
    		System.err.println("path to save the json file is invalid");
    		ioe.printStackTrace();
    	}		
    }

}