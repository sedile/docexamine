package util;

/**
 * this class contains a node for the Graphclass
 * @author sebastian
 *
 */
public class Node {
	
	private final String WORD;
	private final int ID;
	
	/**
	 * The constructor creates a node. Each node requires
	 * a string (word) and a unique id. the rank value gets
	 * a default value of 0.0
	 * @param word the word
	 * @param id a unique id or number
	 */
	public Node(final String word, final int id){
		WORD = word;
		ID = id;
	}
	
	@Override
	public boolean equals(final Object obj){
		if ( obj == null ){
			return false;
		}
		
		if ( obj == this ){
			return true;
		}
		
		if ( obj instanceof Node ){
			Node n = (Node) obj;
			
			String w = n.getWord();
			int id = n.getID();
			//double r = n.getRank();
			if ( WORD.equals(w) && ID == id ){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		return WORD.hashCode();
	}

	/**
	 * get the word of the node
	 * @return word
	 */
	public String getWord(){
		return WORD;
	}
	
	/**
	 * get the id of the node
	 * @return id
	 */
	public int getID(){
		return ID;
	}

}
